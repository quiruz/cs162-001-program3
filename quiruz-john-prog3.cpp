#include <iostream>
#include <cstring>
#include <cctype>
#include <fstream>
using namespace std;

// ****************************************************************************
// John Quiruz, August 1 2023, CS162, Program 3
// This program creates a collection of favorite superheroes and their 
// background information such as name, special powers, admired quality, one
// historical fact, and their arch nemesis. The user will have the option to 
// enter a new superhero to the collection, display superhero profiles, and
// save information to an external data file.
// ****************************************************************************
// Constants
const int HEROES {20};
const int NAME {41};
const int POWER {71};
const int QUALITY {71};
const int FACT {101};
const int ADD {1}; //Menu options...
const int LOAD {2};
const int FIND {3};
const int DISPLAY {4};
const int SAVE {5};
const int EXIT {6};

// Struct
struct superhero // Superhero catalog
{
	char name[NAME];
	char power[POWER];
	char quality[QUALITY];
	char fact[FACT];
	char nemesis[NAME];
};

// Prototypes
int main_menu();
void welcome();
void read_in(superhero & profile);
void find_this(char search_term[]);
//void search_catalog(search_term, superhero profile[], int & catalog_size);
void add_superheroes(superhero profile[], int & catalog_size);
void display_heroes(superhero profile[], int & catalog_size);
void goodbye();

int main()
{
	// Variables
	superhero input[HEROES]; // Catalog of Superhero information
	char search_term[NAME];  // This name is used to find existing 
       	int menu_choice {0};     // hero in the catalog
	int catalog_size {0};

	// Welcome
	welcome();

	// Repeat main menu until the user chooses to quit program
	do 
	{
	menu_choice = main_menu(); // Input for menu

	// Add Superhero to the catalog until user chooses not to
	if (menu_choice == ADD)
		add_superheroes(input, catalog_size);

	// Load an existing catalog from a file
//	else if (menu_choice == LOAD)

	// Find Superhero in the catalog by passing in the arguments:
	// search term, catalog size, and catalog array
	//else if (menu_choice == FIND)
	//	search_catalog(search_term, input, catalog_size);

	// Takes the existing catalog array and displays all of it's 
	// contents such as background info for each Superhero
	else if (menu_choice == DISPLAY)
		display_heroes(input, catalog_size);
	
	// Save current catalog into a file for later use
//	else if (menu_choice == SAVE)

	} while (menu_choice != EXIT); // Ends the program when
                                       //condition is met
	// Goodbye message
	goodbye();

	return 0;
}



// Functions
// Displays the menu interface and provides the user with options
// to choose from and reads in the option choice from the user
int main_menu()
{
	// Variables
	int menu_choice {0};
	
	// Menu interface
	cout << "---------" << endl;
	cout << "Main menu" << endl;
	cout << "---------";
	cout << "\n[1] - ADD Superhero to the catalog" << endl;
	cout << "[2] - LOAD catalog from \'superheroes.txt\'" << endl;
	cout << "[3] - FIND Superhero in catalog" << endl;
	cout << "[4] - DISPLAY Superhero catalog" << endl;
	cout << "[5] - SAVE catalog to \'superheroes.txt\'" << endl;
	cout << "[6] - EXIT program" << endl;

	cout << "\nEnter your choice: "; // Prompt and read menu choice
	cin >> menu_choice;
	cin.ignore(100, '\n');

	while (menu_choice < 1 || menu_choice > 6) // Input validation
	{                                          
		cout << "Please enter correct input [1 - 6]: ";
		cin >> menu_choice;
		cin.ignore(100, '\n');
	}

	return menu_choice; // Will use the choice to navigate through main
}



// Displays welcome message and information about the program
void welcome()
{
	cout << "\n\nSUPERHERO CATALOG" << endl << endl;
}



// Reads in information about one Superhero, which can be used repeatedly
// to proliferate a collection of Superheroes in a struct
void read_in(superhero & info)
{
	cout << "Adding Superhero(s)..." << endl;

	cout << "Enter Superhero name: ";
	cin.get(info.name, NAME, '\n');
	cin.ignore(100, '\n');

	cout << "Enter " << info.name << "\'s super ability: ";
	cin.get(info.power, POWER, '\n');
	cin.ignore(100, '\n');

	cout << "Enter your favorite quality about " << info.name << ": ";
	cin.get(info.quality, QUALITY, '\n');
	cin.ignore(100, '\n');

	cout << "Enter a historical fact about " << info.name << ": ";
	cin.get(info.fact, FACT, '\n');
	cin.ignore(100, '\n');

	cout << "Enter " << info.name << "\'s arch nemesis: ";
	cin.get(info.nemesis, NAME, '\n');
	cin.ignore(100, '\n');
	cout << endl;
}



// Adds information about Superheroes to the catalog and updates the argument
void add_superheroes(superhero profile[], int & catalog_size)
{
	// Variables 
	char choice {'n'};

	// Add information to the catalog and then ask the user if they
	// want to add more Superheroes
	do {
		cout << endl;
		read_in(profile[catalog_size]);
		cout << "Would you like to add another Superhero? 'Y' or 'N': ";
		cin >> choice;
		cin.ignore(100, '\n');
		++catalog_size;
	} while ('Y' == toupper(choice));
	cout << endl;
}



// Displays the information of each Superhero in the catalog
void display_heroes(superhero info[], int & catalog_size)
{	
	// Formatting
	if (catalog_size > 0)
	for (int i {0}; i < catalog_size; ++i)
		cout << "\nDisplaying Superhero catalog..." << endl
		     << "Name: " << info[i].name << endl
		     << "Super ability: " << info[i].power << endl
		     << "Favorite quality: " << info[i].quality << endl
		     << "Historical fact: " << info[i].fact << endl
		     << "Sworn enemy: " << info[i].nemesis << endl;
	else
		cout << "\nThere are no Superheroes in the catalog..." << endl;
	cout << endl;
}




// This function displays a goodbye message before program ends
void goodbye()
{
	cout << "Thank you for using the Superhero Catalog!" << endl << endl;
}
